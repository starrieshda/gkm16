'use strict';

var b = p5.board('/dev/cu.usbmodemFA131', 'arduino');

var pmeter;

var diameter;

function setup() {
    createCanvas(800, 500);


  pmeter = b.pin(0, 'VRES');
  pmeter.read(function(val){
    diameter = val;
    console.log('pmeter read', val)});
  pmeter.range([10, 400]);
  pmeter.threshold(600);

}

function draw() {
  clear();
  ellipse(300, 300, diameter, diameter);
}

function keyPressed() {
  console.log('key pressed!')
}

function mousePressed() {
  console.log('mouse pressed!')

}
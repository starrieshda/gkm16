//help: https://github.com/sarahgp/p5bots/tree/master/examples#variable-resistors
//server starten mit bots-go -d Pfad-zu-Verzeichnis-mit-index.html
//im Browser: localhost:8000
'use strict';

var b = p5.board('COM4', 'arduino'); //ggf. anpassen!
var poti;
var button;


function setup(){
  //docu der Methoden: https://github.com/sarahgp/p5bots/tree/master/src/client#vres-methods
  poti = b.pin(0, 'VRES'); //an A0 angeschlossen
  poti.read(handlePotiInput); //nur im setup, darf nur einmal definiert werden!
  poti.range([0, 400]);
  
  button = b.pin(8, 'BUTTON'); //button an D8 angeschlossen
  createCanvas(800, 400);  
  button.pressed(redEllipse);
  button.released(blueEllipse);
  button.held(greenEllipse, 1000);
  button.read(); //auf Änderungen reagieren
  frameRate(1);
}

function draw(){
	background('white');
}

function handlePotiInput(val){
  //console.log("poti:" + val);
  fill(255, 255, 255);
  rect(0, 0, val, val);
}

function redEllipse() {
    console.log('pressed');
    fill(255, 0, 0);
    ellipse(100, 100, 40, 40);
  }

  function blueEllipse() {
    console.log('released');
    fill(0, 0, 255);
    ellipse(200, 100, 40, 40);
  }

  function greenEllipse() {
    console.log('held')
    fill(0, 255, 136);
    ellipse(300, 100, 40, 40);
  }

  
  


'use strict';

var imgBackground;
var imagesPlayer = [];
var players = [];
var items = [];
var NUMBER_ITEMS = 5;
var imgItem;
var movingStrategies = [mouseMover, randomMover, horizontalMover];
var distCollission = 50;

function preload() {
    imgBackground = loadImage("assets/background.jpg");
    imagesPlayer.push(loadImage("assets/player1.jpg"));
    imagesPlayer.push(loadImage("assets/player2.jpg"));
    imagesPlayer.push(loadImage("assets/player3.jpg"));
    imgItem = loadImage("assets/item.jpg");
    console.log("end of preload");
}
function setup() {
    createCanvas(800, 400);
    background(imgBackground);
    for (var i = 0; i < Math.min(imagesPlayer.length, movingStrategies.length); i++)
        players.push(new Player(i, imagesPlayer[i], movingStrategies[i]));
    for (var i = 0; i < NUMBER_ITEMS; i++)
        items.push(new Item());
    console.log("end of setup");
}

function draw() {
    background(imgBackground);
    for (var i = 0; i < players.length; i++) {
        //check collection of items
        for (var k = 0; k < items.length; k++) {
            var d = distFixed(items[k].position, players[i].position);
            if (d < distCollission) {
                players[i].scored();
                items[k].eaten();
            }
            items[k].draw();
        }
        players[i].update();
        players[i].draw();
    }
}

function distFixed(v1, v2) {
    return Math.sqrt((v2.x - v1.x) * (v2.x - v1.x) + (v2.y - v1.y) * (v2.y - v1.y));
}

function mouseMover(x, y) {
    return {x: mouseX, y: mouseY};
}

function randomMover(xOld, yOld) {
    var maxSpeed = 10;
    return {x: constrain(xOld + random(-maxSpeed, maxSpeed), 0, width), y: constrain(yOld + random(-maxSpeed, maxSpeed), 0, height)};
}

function horizontalMover(xOld, yOld) {
    return {x: xOld > width ? 0 : xOld + 1, y: yOld};
}